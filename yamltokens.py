class YAMLToken:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return "%s: %s" % (self.__class__.__name__, self.__dict__)

    def __str__(self):
        return self.__repr__()

class NewDocumentToken(YAMLToken):
    pass

class AttributeSpecifyToken(YAMLToken):
    pass

class ListInitToken(YAMLToken):
    pass

class RestOfLineToken(YAMLToken):
    pass

