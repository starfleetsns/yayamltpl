#!/usr/bin/env python3
"""
# Tag user guide
## How to read the explanations
TODO

- `<a|b>` means extending the dictionary `a` with the elements in dictionary `b`.
"""

from yamlast import YAMLTag, register_tag
from yamlexceptions import YAMLRuntimeError
from parser import YAMLParser
import os, sys
from warnings import warn

yaml = YAMLParser()

class TTag(YAMLTag):
    def __init__(self, calledwith, argument, **kwargs):
        if calledwith not in self.__class__.accepts:
            raise YAMLRuntimeError(f"Tag {self.__class__.__name__} called with type {callewith} but it accepts only {self.__class__.accepts}")
        self.calledwith = calledwith
        self.argument = argument
        self.result = None

    def flatten(self):
        if hasattr(self.result, "flatten"):
            return self.result.flatten()
        return self.result
        
def get_filename(cur_cwd, filename):
    return filename if os.path.isabs(filename) else os.path.join(cur_cwd, filename)

@register_tag("eval")
@register_tag("varexpr")
class EvalTag(TTag):
    """Tag: !eval !varexpr
    Performs a standard `eval` construct.
    
    Accepts: Scalar

    Possible semantics:
    - `eval argument in current context`
    """

    accepts = ["scalar"]
    def traverse_apply(self, ctx, **kwargs):
        self.result = eval(self.argument.flatten(), dict(ctx, **globals()))

@register_tag("template")
class TemplateTag(TTag):
    """Tag: !template
    Loads a templated yaml and substitutes at the current document point.
    
    Accepts: Mapping

    Possible semantics:
    - `use <defaults|arguments> as context in (loaded document from <filename>)`
    - `map <arguments> (use <defaults|.> as context in (loaded document from <filename>)`
    - `use <defaults|arguments> as context in <template>`
    - `map <arguments> (use <defaults|.> as context in <template>)`
    """
    accepts = ["mapping"]
    def traverse_apply(self, ctx, cwd, **kwargs):
        template = None
        if "template" in self.argument.mapping:
            template = self.argument.mapping["template"]
        elif "filename" in self.argument.mapping:
            fname = get_filename(cwd, self.argument.mapping["filename"])
            template = yaml.load_all(fname)
            cwd = os.path.dirname(fname)

        # TODO
            
@register_tag("if")
@register_tag("ifexpr")
class IfTag(TTag):
    """
    Performs a standard `if` construct.
    
    Accepts: Mapping

    Possible semantics:
    - `if(eval <condition> in current context) <is_true> else <is_false>`
    """
    accepts = ["mapping"]
    def traverse_apply(self, ctx, **kwargs):
        condition = eval(self.argument.mapping["condition"].flatten(), dict(ctx, **globals()))
        if condition:
            self.result = self.argument.mapping["is_true"](ctx=ctx, **kwargs)
        else:
            self.result = self.argument.mapping["is_false"](ctx=ctx, **kwargs)
        
if __name__ == "__main__":
    from sys import argv
    pfile = yaml.load_all(argv[1])
    cwd = os.path.dirname(argv[1])
    ctx= {"manuele": "cusumano", "safevar": {"isobj": "lol"}, "destinatario": "a noi"}
    try:
        print(pfile(ctx=ctx, cwd=cwd).flatten())
    except Exception as e:
        if hasattr(e, "__stacktrace__"):
            print("\n=== EXCEPTION OCCURRED ===")
            for line in e.__stacktrace__:
                print(line)
        else:
            raise e
