class NoObjectFound(Exception):
    pass

class YAMLError(Exception):
    pass

class YAMLSyntaxError(YAMLError, SyntaxError):
    pass

class YAMLRuntimeError(YAMLError, RuntimeError):
    pass

