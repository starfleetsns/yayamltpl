class peekable:
    def __init__(self, iterator):
        self.iterator = iterator
        self.issaved = False
        self.saved_item = None

    def __iter__(self):
        iter(self.iterator)
        return self

    def peek(self):
        if not self.issaved:
            self.issaved = True
            self.saved_item = next(self.iterator)
        return self.saved_item            
    
    def __next__(self):
        if self.issaved:
            self.issaved = False
            return self.saved_item
        return next(self.iterator)

def debug_returned_values(fn):
    def wrapped(*args, **kwargs):
        result = fn(*args, **kwargs)
        print(f"{fn.__name__} return {repr(result)}")
        return result
    return wrapped

def debug_yielded_values(gen):
    def wrapped(*args, **kwargs):
        for result in gen(*args, **kwargs):
            print(f"{gen.__name__} yield {repr(result)}")
            yield result
    return wrapped

def get_fileline(filename, lineno):
    lines = open(filename, "r").readlines()
    if len(lines) >= lineno:
        return lines[lineno-1][:-1]
    return "@EOF+%d" % (lineno -1  - len(lines))
