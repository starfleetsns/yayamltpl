#!/usr/bin/env python3

import os, sys
import ruamel.yaml as yaml
from ruamel.yaml import YAML, Loader
from ruamel.yaml.compat import StringIO
from warnings import warn
from base64 import b64decode, b64encode

class MyYAML(YAML):
    def dump(self, data, stream=None, **kwargs):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        YAML.dump(self, data, stream, **kwargs)
        if inefficient:
            return stream.getvalue()

    def dump_all(self, data, stream=None, **kwargs):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        YAML.dump_all(self, data, stream, **kwargs)
        if inefficient:
            return stream.getvalue()

yaml_loader = MyYAML()

class Varexpr(yaml.YAMLObject):
    yaml_tag = u'!varexpr'

    def __init__(self, expr):
        self.expr = expr

    def __call__(self, ctx, cwd):
        try:
            evalctx = dict(ctx, **globals())
            return eval(self.expr, evalctx)
        except Exception as e:
            print(f"Exception: {e}")
            print(f"In: !varexpr {self.expr}")
            print(f"Working dir: {cwd}")
            print(f"Context: {ctx}")
            sys.exit(127)


def varexpr_representer(dumper, data):
    return dumper.represent_scalar(Varexpr.yaml_tag, data.expr)


def varexpr_constructor(loader, node):
    expr = loader.construct_scalar(node)
    return Varexpr(expr)


yaml.add_representer(Varexpr, varexpr_representer)
yaml.add_constructor(Varexpr.yaml_tag, varexpr_constructor)


class Template(yaml.YAMLObject):
    yaml_tag = u'!template'

    def __init__(self, filename, arguments):
        self.filename = filename
        self.arguments = arguments

    def __call__(self, ctx, cwd, multiple=False):
        if type(self.arguments) == list:
            parsed_arguments = [substitute(arg, ctx, cwd) for arg in self.arguments]
        else:
            parsed_arguments = substitute(self.arguments, ctx, cwd)

        parsed_filename = substitute(self.filename, ctx, cwd) if "filename" in self.__dict__ else None
        inline_template = self.template if "template" in self.__dict__ else None

        if os.path.isabs(parsed_filename):
            newcwd = os.path.dirname(parsed_filename)
        else:
            newcwd = os.path.join(cwd, os.path.dirname(parsed_filename))

        templatefile = open(os.path.join(cwd, parsed_filename), "r") if parsed_filename is not None else None

        if multiple is False:
            template = yaml.load(templatefile, Loader=Loader) if parsed_filename is not None else inline_template
                
            if type(self.arguments) == list:
                newctxs = [dict(ctx, **arg) for arg in parsed_arguments]
                result = [substitute(template, newctx, newcwd) for newctx in newctxs]
            else:
                newctx = dict(ctx, **parsed_arguments)
                result = substitute(template, newctx, newcwd)
        else:
            templates = yaml.load_all(templatefile, Loader=Loader)
            newctx = dict(ctx, **parsed_arguments)
            result = []
            for template in templates:
                if isinstance(template, DocumentMap):
                    for document in template(newctx, newcwd):
                        result.append(document)
                else:
                    result.append(substitute(template, newctx, newcwd))
        if parsed_filename is not None:
            templatefile.close()
        return result


class Switchexpr(yaml.YAMLObject):
    yaml_tag = u'!switchexpr'
    
    def __init__(self, expression, outcomes, default):
        self.expression = expression
        self.outcomes = outcomes
        self.default = default

    def __call__(self, ctx, cwd):
        result = eval(self.expression, globals(), ctx)
        if "default" in self.__dict__ and result not in self.outcomes:
            return substitute(self.default, ctx, cwd)
        return substitute(self.outcomes[result], ctx, cwd)
        

class Joindicts(yaml.YAMLObject):
    yaml_tag = u'!joindicts'
    
    def __init__(self, dicts):
        self.dicts = dicts

    def __call__(self, ctx, cwd):
        dicts = [substitute(dct, ctx, cwd) for dct in self.dicts]
        joined = dict()
        for dct in dicts:
            joined = dict(joined, **dct)
        return joined
        
def joindicts_representer(dumper, data):
    return dumper.represent_sequence(Joindicts.yaml_tag, data.dicts)


def joindicts_constructor(loader, node):
    dicts = loader.construct_sequence(node, deep=True)
    return Joindicts(dicts)


yaml.add_representer(Joindicts, joindicts_representer)
yaml.add_constructor(Joindicts.yaml_tag, joindicts_constructor)


class Joinlists(yaml.YAMLObject):
    yaml_tag = u'!joinlists'
    
    def __init__(self, lists):
        self.lists = lists

    def __call__(self, ctx, cwd):
        lists = [substitute(lst, ctx, cwd) for lst in self.lists]
        joined = []
        for elem in lists:
            if type(elem) == list:
                for el in elem:
                    joined.append(el)
            elif elem is not None:
                joined.append(elem)
        return joined
        
def joinlists_representer(dumper, data):
    return dumper.represent_sequence(Joinlists.yaml_tag, data.lists)


def joinlists_constructor(loader, node):
    lists = loader.construct_sequence(node, deep=True)
    return Joinlists(lists)


yaml.add_representer(Joinlists, joinlists_representer)
yaml.add_constructor(Joinlists.yaml_tag, joinlists_constructor)


class Ifexpr(yaml.YAMLObject):
    yaml_tag = u'!ifexpr'
    
    def __init__(self, condition, if_true, if_false):
        self.condition = condition
        self.if_true = if_true
        self.if_false = if_false

    def __call__(self, ctx, cwd):
        result = eval(self.condition, globals(), ctx)
        if result:
            return substitute(self.if_true, ctx, cwd)
        else:
            return substitute(self.if_false, ctx, cwd)


class Deprecated(yaml.YAMLObject):
    yaml_tag = u'!deprecated'

    def __init__(self):
        pass

    def __call__(self, ctx, cwd):
        message = substitute(self.message, ctx, cwd)
        context = substitute(self.context, ctx, cwd) if "context" in self.__dict__ else {}
        warn(DeprecationWarning(message, context))
        return substitute(self.object, ctx, cwd)


class YamlUserError(RuntimeError):
    pass
    

class YamlError(yaml.YAMLObject):
    yaml_tag = u'!error'

    def __init__(self):
        pass

    def __call__(self, ctx, cwd):
        message = substitute(self.message, ctx, cwd)
        context = substitute(self.context, ctx, cwd) if "context" in self.__dict__ else {}
        raise YamlUserError(message, context)


class Let(yaml.YAMLObject):
    yaml_tag = u'!let'

    def __call__(self, ctx, cwd):
        name = substitute(self.name, ctx, cwd)
        value = substitute(self.value, ctx, cwd)
        newctx = dict(ctx, **{name: value})
        return substitute(self.__dict__["in"], newctx, cwd)


class SetDefaults(yaml.YAMLObject):
    yaml_tag = u'!setdefaults'

    def __call__(self, ctx, cwd):
        defaults = substitute(self.defaults, ctx, cwd)
        newctx = dict(defaults, **ctx)
        return substitute(self.__dict__["in"], newctx, cwd)


class DocumentMap(yaml.YAMLObject):
    yaml_tag = u'!documentmap'
    
    def __init__(self, filename, instances, defaults):
        self.filename = filename
        self.instances = instances
        self.defaults = defaults

    def __call__(self, ctx, cwd):
        default_arguments = substitute(self.defaults, ctx, cwd) if "defaults" in self.__dict__ else {}
        documents = []
        instances = substitute(self.instances, ctx, cwd)
        if instances is not None:
            for instance in instances:
                instance_arguments = substitute(instance, ctx, cwd) if instance is not None else {}
                joined_arguments = dict(default_arguments, **instance_arguments)
                evaluated_templates = Template(self.filename, joined_arguments)(ctx, cwd, multiple=True)
                documents += evaluated_templates
        return documents


def substitute(obj, ctx, cwd):
    if type(obj) == dict:
        return {key: substitute(val, ctx, cwd) for key, val in obj.items()}
    elif type(obj) == list:
        return [substitute(elem, ctx, cwd) for elem in obj]
    elif isinstance(obj, Varexpr):
        return obj(ctx, cwd)
    elif isinstance(obj, Template):
        return obj(ctx, cwd)
    elif isinstance(obj, Switchexpr):
        return obj(ctx, cwd)
    elif isinstance(obj, Ifexpr):
        return obj(ctx, cwd)
    elif isinstance(obj, Deprecated):
        return obj(ctx, cwd)
    elif isinstance(obj, Joindicts):
        return obj(ctx, cwd)
    elif isinstance(obj, Joinlists):
        return obj(ctx, cwd)
    elif isinstance(obj, YamlError):
        return obj(ctx, cwd)
    elif isinstance(obj, Let):
        return obj(ctx, cwd)
    elif isinstance(obj, SetDefaults):
        return obj(ctx, cwd)
    elif isinstance(obj, DocumentMap):
        raise SyntaxError("A !documentmap can only be inserted at a document top-level.")
    else:
        return obj

def apply_template(filename, ctx):
    cwd = os.path.dirname(filename)
    with open(filename, "r") as intpl:
        filescontents = yaml.load_all(intpl, Loader=Loader)
        substituted = []
        for filecont in filescontents:
            if isinstance(filecont, DocumentMap):
                for document in filecont(ctx, cwd):
                    substituted.append(document)
            else:
                substituted.append(substitute(filecont, ctx, cwd))
        return yaml_loader.dump_all(substituted)
    
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="A YAML templater language.")
    parser.add_argument("template", type=str, help="The template file")
    parser.add_argument("context", type=str, nargs="*", help="The context files")
    args = parser.parse_args()

    # Introduce environment variables starting with TPL_ inside of the context
    environment = {**os.environ}
    env_vars = {key[4:]: yaml.load(value, Loader=yaml.UnsafeLoader)
                for key, value in environment.items()
                if key.startswith("TPL_")}
    
    ctx = {
        **env_vars,
        "_filters": {
            "b64decode": b64decode,
            "b64encode": lambda arg: str(b64encode(arg.encode()), 'utf-8'),
            "yaml_load": lambda x: yaml.load(x, Loader=yaml.UnsafeLoader),
        },
        # TODO: Add filters to the environment
    }
    for context_file in args.context:
        newctx = yaml.load(open(context_file, "r"), Loader=yaml.UnsafeLoader)
        ctx = {**ctx, **newctx}
    
    result = apply_template(args.template, ctx)
    sys.stdout.write(result)
    sys.stdout.flush()


