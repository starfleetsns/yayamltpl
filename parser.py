from re import match as rematch
from yamlast import ParsedFile, ParsedDocument, ParsedAttribute, ParsedTagObject, ParsedMapping, ParsedSequence, ParsedScalar, YAMLTag, register_tag
from yamlexceptions import NoObjectFound, YAMLError, YAMLSyntaxError, YAMLRuntimeError
from yamltokens import NewDocumentToken, AttributeSpecifyToken, ListInitToken, RestOfLineToken
from yamlutils import peekable, debug_returned_values, debug_yielded_values

class YAMLParser:
    def __init__(self):
        pass

    # @debug_returned_values
    def build_object(self, ptokens, indentation_level=0):
        # object_type can be "sequence" or "mapping" or "scalar"
        object_type = None
        current_object = None
        while True:
            tok = ptokens.peek()
            # Skip it if it is an empty line
            if isinstance(tok, RestOfLineToken) and not tok.hasvalue and tok.tag is None and tok.key is None:
                next(ptokens)
                continue

            # TODO: Add multiple possible indentation levels that are ok for us (alignment of tagged dictionaries
            # Check that the token is either at the expected indentation level or below, but not above
            if tok.indented > indentation_level:
                raise YAMLSyntaxError(f"Token in file {tok.__file__} in line {tok.__line__} is not properly indented")
            elif tok.indented < indentation_level:
                # We should send back what we already have
                if current_object is not None:
                    return current_object
                # Otherwise we were called to parse an object but we found nothing, so it is an empty mapping
                # But it may also be the global object (only if indentation_level = 0), so we check that
                if indentation_level == 0:
                    raise NoObjectFound()
                return ParsedMapping(mapping={}, __file__=tok.__file__, __line__=tok.__line__, __comment__=tok.__comment__, __column__=tok.__column__)
                
            if isinstance(tok, ListInitToken):
                if object_type is None:
                    # We are then looking to build a list of equally indented data
                    current_object = ParsedSequence(sequence=[], __line__=tok.__line__, __file__=tok.__file__,
                                                    __column__=tok.__column__, __comment__=tok.__comment__)
                    object_type = "sequence"

                if object_type == "sequence":
                    next(ptokens) # Consume current token
                    current_object.sequence.append(self.build_object(ptokens, indentation_level+2))
                else:
                    raise YAMLSyntaxError(f"List initiation in file {tok.__file__} at line {tok.__line__} is misplaced")
            elif isinstance(tok, RestOfLineToken):
                # Possible cases of this parsing branch:
                # 1. Has key and value --> Start or continuing of a mapping, add to it
                # 2. Has only key --> Add to the mapping and call a submapping
                # 3. Has only value --> Return the value object, which is already parsed
                if tok.key is not None:
                    if object_type is None:
                        current_object = ParsedMapping(mapping={}, __line__=tok.__line__, __file__=tok.__file__,
                                                       __column__=tok.__column__, __comment__=tok.__comment__)
                        object_type = "mapping"

                    if object_type == "mapping":
                        if tok.hasvalue:
                            # Case 1: Check if it has a tag, and return the mapping
                            parsed_scalar = ParsedScalar(value=tok.value, __line__=tok.__line__, __file__=tok.__file__,
                                                         __column__=tok.__column__, __comment__=tok.__comment__)
                            if tok.tag is not None:
                                current_object.mapping[tok.key] = ParsedTagObject(tag=tok.tag, calledwith="scalar", argument=parsed_scalar, __line__=tok.__line__, __file__=tok.__file__, __column__=tok.__column__)
                            else:
                                current_object.mapping[tok.key] = parsed_scalar
                            next(ptokens)
                        else:
                            # Case 2: Add the thing to the mapping and call a submapping parser
                            if tok.tag is not None:
                                next(ptokens)
                                subobject = self.build_object(ptokens, indentation_level=indentation_level+2)
                                if isinstance(subobject, ParsedMapping):
                                    current_object.mapping[tok.key] = ParsedTagObject(tag=tok.tag, calledwith="mapping", argument=subobject, __line__=tok.__line__, __file__=tok.__file__, __column__=tok.__column__)
                                elif isinstance(subobject, ParsedSequence):
                                    current_object.mapping[tok.key] = ParsedTagObject(tag=tok.tag, calledwith="sequence", argument=subobject, __line__=tok.__line__, __file__=tok.__file__, __column__=tok.__column__)
                                else:
                                    raise YAMLSyntaxError(f"Multiline-argument to tag {tok.tag} in file {tok.__file__} at line {tok.__line__} is neither a sequence nor a mapping")
                            else:
                                next(ptokens)
                                current_object.mapping[tok.key] = self.build_object(ptokens, indentation_level=indentation_level+2)
                    else:
                        raise YAMLSyntaxError(f"Object in file {tok.__file__} under line {tok.__line__} is unaligned")
                else: # tok.key is None, so it can only be a scalar
                    if object_type is not None:
                        raise YAMLSyntaxError(f"Argument in file {tok.__file__} at line {tok.__line__} is scalar, while we expected a {object_type}")

                    next(ptokens) # It will be consumed no matter what.
                    if tok.hasvalue:
                        parsed_scalar = ParsedScalar(value=tok.value, __line__=tok.__line__, __file__=tok.__file__,
                                                     __column__=tok.__column__, __comment__=tok.__comment__)
                        if tok.tag is not None:
                            return ParsedTagObject(tag=tok.tag, calledwith="scalar", argument=parsed_scalar, __line__=tok.__line__, __file__=tok.__file__, __column__=tok.__column__)
                        else:
                            return parsed_scalar
                    else: # Beware that there may be a tag without value and key
                        if tok.tag:
                            return ParsedTagObject(tag=tok.tag, calledwith="nothing", argument=None, __line__=tok.__line__, __file__=tok.__file__, __column__=tok.__column__)
            else:
                if current_object is not None:
                    return current_object
                raise NoObjectFound()
    
    def build_documents(self, ptokens):
        documents = []
        current_document_attributes = []
        current_document_mainobject = None
        current_document_assignedobj = False
        current_document_number = 0
        try:
            tok = ptokens.peek()
            current_file = tok.__file__
            while True:
                tok = ptokens.peek()
                if isinstance(tok, NewDocumentToken):
                    documents.append(ParsedDocument(attributes=current_document_attributes,
                                                    main=current_document_mainobject,
                                                    __file__=current_file,
                                                    __docnum__=f"#{current_document_number}"))
                    current_document_attributes = []
                    current_document_mainobject = None
                    current_document_assignedobj = False
                    current_document_number += 1
                    next(ptokens)
                elif isinstance(tok, AttributeSpecifyToken):
                    attribute = ParsedAttribute(__line__=tok.__line__, __file__=tok.__file__, __column__=tok.__column__,
                                                name=tok.attribute_name, value=tok.attribute_value)
                    current_document_attributes.append(attribute)
                    next(ptokens)
                else:
                    try:
                        current_document_mainobject = self.build_object(ptokens) # Try to parse also blank lines at the end
                        if current_document_assignedobj:
                            raise YAMLSyntaxError(f"Multiple main objects in document inside {tok.__file__} at line {tok.__line__}")
                        current_document_assignedobj = True
                    except NoObjectFound:
                        pass # Nothing was found (empty line), no need to worry.

        except StopIteration:
            return ParsedFile(documents=documents, __file__=current_file, __docnum__="",)

    def ast_builder(self, tokens):
        return self.build_documents(peekable(tokens))

    def parse_value(self, value_string):
        regexes = {
            r"^(None|null)$": lambda x: None,
            r"^[Tt]rue$": lambda x: True,
            r"^[Ff]alse$": lambda x: False,
            r"^(?P<number>[0-9]+)$": lambda x: int(x.group("number")),
            r"^\[\]$": lambda x: [],
        }
        if value_string is None:
            return None
        for regex, get_value in regexes.items():
            match = rematch(regex, value_string)
            if match:
                return get_value(match)
        return value_string

    # @debug_yielded_values
    def tokenizer(self, filename):
        document_change_line = r'^---\s*$'
        attribute_specify_line = r'^\%(?P<attribute_name>[a-zA-Z0-9\_]+)\:\ (?P<attribute_value>.*)$'
        normal_line = r'^(?P<initial_spaces>\ *)(?P<list_inits>(\-\ )+)?((?P<key>[a-zA-Z0-9\_]+)\:)?\ *(\!(?P<tag>[a-zA-Z0-9\_]+))?\ *(?P<value>[^\#]+?)?\ *(\#\ *(?P<comment>.*))?$'
        lineno = 0
        for line in open(filename, "r").readlines():
            lineno += 1
            line = line[:-1] # Remove final newline character
            # print(lineno, line)
            dcmatch = rematch(document_change_line, line)
            asmatch = rematch(attribute_specify_line, line)
            nmatch = rematch(normal_line, line)
            if dcmatch:
                yield NewDocumentToken(__file__=filename, __line__=lineno, __column__=0, indented=0)
            elif asmatch:
                yield AttributeSpecifyToken(__file__=filename, __line__=lineno, __column__=1,
                                            indented=0,
                                            attribute_name=asmatch.group("attribute_name"),
                                            attribute_value=self.parse_value(asmatch.group("attribute_value")))
            elif nmatch:
                # Keep track of current amount of indentation
                current_indentation = len(nmatch.group("initial_spaces"))
                has_lists = nmatch.group("list_inits") is not None
                has_comment = nmatch.group("comment") is not None
                # Emit lists initializations
                if has_lists:
                    inits = int(len(nmatch.group("list_inits")) / 2)
                    for i in range(inits):
                        if has_comment and i == 0:
                            yield ListInitToken(__file__=filename, __line__=lineno, __column__=current_indentation,
                                                indented=current_indentation, __comment__=nmatch.group("comment"))
                        else:
                            yield ListInitToken(__file__=filename, __line__=lineno, __column__=current_indentation,
                                                indented=current_indentation, __comment__=None)
                        current_indentation += 2

                remaining_comment = nmatch.group("comment") if not has_lists else None
                yield RestOfLineToken(__file__=filename, __line__=lineno, __column__=current_indentation,
                                      indented=current_indentation,
                                      key=nmatch.group("key"),
                                      tag=nmatch.group("tag"),
                                      hasvalue=nmatch.group("value") is not None,
                                      value=self.parse_value(nmatch.group("value")),
                                      __comment__=remaining_comment)
            else:
                raise YAMLSyntaxError(f"Wrong syntax at line {lineno} in file {filename}")
        yield NewDocumentToken(__file__=filename, __line__=lineno+1, __column__=0, indented=0)

    def load_all(self, filename):
        tokens = self.tokenizer(filename)
        ast = self.ast_builder(tokens)
        return ast

    def load(self, filename):
        documents = self.load_all(filename)
        if len(documents) != 1:
            raise YAMLRuntimeError("Tried to load a single-document YAML but it has many")
        return documents[0]
