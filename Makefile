dist/yayamltpl: venv_package
	. venv_package/bin/activate && \
	pyinstaller --onefile -y --clean --name yayamltpl tpl.py

dist/tpl-linux-x64.zip: tpl.dist
	mkdir -p dist && mv tpl.dist tpl-linux-x64 && \
	zip -r dist/tpl-linux-x64.zip tpl-linux-x64 && rm -rf tpl-linux-x64

venv_package: requirements-package.txt
	python3 -m venv venv_package && . venv_package/bin/activate && \
	pip install -r requirements-package.txt && pip install -r requirements.txt
