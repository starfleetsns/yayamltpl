# Yet Another YAML Templater
In this templater we define some special YAML tag that let you include other YAML in files and template it with some variables.
A brief documentation about the usage mode of the various operators follows.

## Executable Download and Usage
We provide a one-file executable for linux environments produced with PyInstaller.
The latest (development) version is available at
https://gitlab.com/starfleetsns/yayamltpl/-/jobs/artifacts/master/raw/yayamltpl?job=build_continuous
and stable (tagged) releases are available at
https://gitlab.com/starfleetsns/yayamltpl/-/jobs/artifacts/v0.1.0/raw/yayamltpl?job=build_tag
by changing v0.1.0 with the tag you are interested in.

The usage is very simple:
```
yayamltpl TEMPLATE_FILE [CONTEXT_FILES ...]
```
where the files are in yaml syntax.
The output is on stdout.
Later context files override variables in earlier ones.

Moreover environment variables starting with `TPL_` are yaml-parsed and injected into the environment.
```
export TPL_cusumano="{'manuele': 3}"
```

## Execution model
The documents are templated in the same order in which they are defined, and all tags are executed in the order in which they are defined.
When a tag is encountered, it may choose to evaluate only certain parts of its arguments, as it is the case with the various conditional tags.
No tag can perform side-effects.

## Tag list
### !varexpr
Evaluates a python expression using `eval` in the current context.

Accepts: scalar.
```yaml
# The expression is evaluated inside of the current context
!varexpr 12 ** 3 + 1 == 1729
```

Caveat: remember to quote the string if it contains quotes or starts with braces.

### !template
Evaluates a template included from another YAML file in the current context, modified in some way, and substitutes the evaluation at the current point in the current document.

Accepts: mapping.
```yaml
!template
  # filename can be both a relative and an absolute path
  filename: ../templates/manuele.yml
  # As alternative to filename, one can specify an inline template
  template:
    topthree: !varexpr var1[:3]
	other_results: !varexpr var3
  # arguments can also be a single dictionary, in which case a single instance is generated
  arguments:
    - var1: new_value
	- var3: other_value
```

Caveat: the included YAML file must consist of a single document.

### !documentmap
Inserts documents generated from a template after the current document.
In other words, is like a `!template` tag for documents, but can insert many documents into the current stream.

Accepts: mapping.
```yaml
!documentmap
  filename: ../templates/cusu.yml
  # defaults changes the value wrt the current context but the values 
  # can be overridden by those declared in instances.
  defaults:
    var2: new_default
  # instances is like arguments in !template
  instances:
    - var2: override_default
	- var3: other_value
```

Caveat: can only appear at the top-level of a document.

### !ifexpr
Allows to execute only one of the two branches, depending on an evaluated condition.

Accepts: mapping.
```yaml
!ifexpr
  condition: 'manuele == "cusumano"'
  if_true: Manuele is cusumano!
  if_false:
    manuele_is: !varexpr manuele
```

### !joindicts
Allows to join various dictionaries together, in a single dictionary.
It allows to substitue the `<<:` syntax of YAML for key merging.

Accepts: sequence.
```yaml
!joindicts
  - a: 3
    b: 4
  - a: 9
    c: 3
```

Caveat: later dictionaries override the variables of earlier ones.

### !joinlists
Allows to join various lists in a single one.

Accepts: sequence
```yaml
!joinlists
  - - first element
    - second element
  # Note that if an argument consists of something that is not a list,
  # it is added "as is" to the resulting list.
  - third element
  - - fourth element
```

### !setdefaults
Allows to set a default value to a context variable if it is not set.

Accepts: mapping.
```yaml
!setdefaults
  defaults:
    var1: default_value
  in:
    - something
	- !varexpr var1
```

### !switchexpr
Performs a classical `switch` construct, evaluating an expression.

Accepts: mapping.
```yaml
!switchexpr
  expression: person_surname
  outcomes:
    cusumano: +100
	balboni: +100
	zannier: -200
  # default is a catch-all expression, which can be omitted
  default: -100
```

### !deprecated
Prints a deprecation warning when evaluated.

```yaml
!deprecated
  # message is a short description of the deprecation
  message: This module is deprecated because of cusumano.
  # context is every information that we want to add to the exception
  context:
    reason: Cusumano asked it kindly.
	date: 09/11/2001
```

### !error
Raises an exception when evaluated.
Can be used inside an `!if` condition to stop execution if some variables are wrongly setted.

```yaml
# The parameters are the same as the !deprecated tag
!error
  message: Cannot compute the factorial of a negative number
  context:
    number: !varexpr number
	reason: I don't know about the gamma function
```

### !let
Allows to insert a new variable in the context of a subtree, evaluating to an expression.
Can be used to shorten the name of the relevant variables.

```yaml
!let
  name: short_name
  value: !varexpr 'interesting_dictionary["subkey"][variable_indexing].value'
  in:
    - !varexpr short_name
```

## Tips and Tricks
### !documentmap inside a pure conditional
Suppose you want to set a default inside the expansion of a templatemap, which is not possible since templatemap can only occur at the top level.
```yaml
!setdefaults
  defaults:
    cusu: mano
	lol: tipo
  in: !templatemap
        filename: somefile.yml
		instances:
		  - lol: False
```
You may bring the `!setdefaults` call inside of the template file `somefile.yml`.

### Unbounded recursion
Wrapping a `!template` referring to the file itself inside an `!if` conditional, one is able to create arbitrary recursion.

Here a possible version of a `factorial.yml` template:
```yaml
!setdefaults
defaults:
  maxnum: 10
  accum: 1
  num: 1
in: !ifexpr
  condition: num > maxnum
  if_true: {}
  if_false: !joindicts
      - !varexpr '{str(num): num * accum}'
      - !template
        filename: factorial.yml
        arguments:
          num: !varexpr num + 1
          accum: !varexpr accum * num
```
