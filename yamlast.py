from yamlexceptions import YAMLError
from yamlutils import get_fileline

# Definitions of parsed YAML Objects
class ParsedYAMLObject:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return "%s: %s" % (self.__class__.__name__, self.__dict__)
        
    def replicate(self):
        return self.__class__(**dict(self.__dict__))

    def description(self):
        return "%s: %s" % (self.__class__.__name__, get_fileline(self.__file__, self.__line__))
    
    def __call__(self, **kwargs):
        newself = self.replicate()
        newself.traverse_apply(**kwargs)
        return newself

class ParsedFile(ParsedYAMLObject):
    def flatten(self):
        return [document.flatten() for document in self.documents]
    
    def traverse_apply(self, **kwargs):
        self.documents = [document(**kwargs) for document in self.documents]
    
class ParsedDocument(ParsedYAMLObject):
    def flatten(self):
        return self.main.flatten()

    def traverse_apply(self, **kwargs):
        self.main = self.main(**kwargs)

class ParsedAttribute(ParsedYAMLObject):
    pass

class ParsedSequence(ParsedYAMLObject):
    def flatten(self):
        return [element.flatten() for element in self.sequence]

    def traverse_apply(self, **kwargs):
        self.sequence = [element(**kwargs) for element in self.sequence]

class ParsedMapping(ParsedYAMLObject):
    def flatten(self):
        return {key: value.flatten() for key, value in self.mapping.items()}

    def traverse_apply(self, **kwargs):
        self.mapping = {key: value(**kwargs) for key, value in self.mapping.items()}

class ParsedScalar(ParsedYAMLObject):
    def flatten(self):
        return self.value
    
    def __repr__(self):
        return repr(self.value)

    def __str__(self):
        return repr(self.value)

    def traverse_apply(self, **kwargs):
        pass # Does nothing since it is a scalar

TagClasses = {}
def register_tag(tag_name):
    def decorator(cls):
        if tag_name in TagClasses:
            raise YAMLError(f"Tag !{tag_name} is already defined to be the class {TagClasses[tag_name].__name__}")
        TagClasses[tag_name] = cls
        return cls
    return decorator

class YAMLTag:
    def __repr__(self):
        return "%s: %s" % (self.__class__.__name__, self.__dict__)
        
    def replicate(self):
        return self.__class__(**dict(self.__dict__))

    def description(self):
        return "%s: %s" % (self.__class__.__name__, get_fileline(self.__file__, self.__line__))
    
    def __call__(self, **kwargs):
        try:
            newself = self.replicate()
            newself.traverse_apply(**kwargs)
            return newself
        except Exception as e:
            if not hasattr(e, "__stacktrace__"):
                e.__stacktrace__ = [f"{repr(e)}"]
            if hasattr(self, "__line__"):
                e.__stacktrace__.append(f"[{self.__file__}:{self.__line__}] {self.description()}")
            else: # It is either a file or a document
                e.__stacktrace__.append(f"[{self.__file__}{self.__docnum__}] {self.__class__.__name__}")
            raise e
    
class ParsedTagObject(ParsedYAMLObject):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if self.tag not in TagClasses:
            raise YAMLError(f"No user tag class defined for tag {self.tag}")
        self.tagobj = TagClasses[self.tag](calledwith=kwargs["calledwith"], argument=kwargs["argument"])
        self.tagobj.__dict__.update(**{key: value for key, value in kwargs.items() if key.startswith("__")})
        
    def flatten(self):
        return self.tagobj.flatten()

    def traverse_apply(self, **kwargs):
        self.tagobj = self.tagobj(**kwargs)
